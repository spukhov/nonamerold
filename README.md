# Дружище, а не потерялся ли ты? #

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Для локального деплоя должен быть установлен [Gradle](https://www.gradle.org/)  
После клонирования проекта, переходим в папочку, делаем *git checkout develop* и выполняем команду *gradle jettyRun*.
Вот и все. После этого приложение будет развернуто на порту 8080.

Осторожно: в скрипте app.js есть переменная domain, которая должна указвать на домен, где локально развернут проект (иначе джаваскрипт не сможет делать http-запросы)


### Team members ###

@spukhov, @ilopushen