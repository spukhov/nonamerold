package dao;

import com.headlezz.config.SpringMongoConfig;
import com.headlezz.mongo.Credentials;
import com.headlezz.mongo.UserData;
import com.headlezz.service.UserService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringMongoConfig.class)
public class UserServiceTest {
    @Autowired
    private  UserService userService;
    private static Credentials testUser;
    private static UserData testUserData;
    private static String TEST_USER_EMAIL = "test@test.ua";
    private static String TEST_USER_PASSWORD = "testpass";

    @BeforeClass
    public static void init(){
        testUser = new Credentials();
        testUserData = new UserData();
        testUser.setEmail(TEST_USER_EMAIL);
        testUser.setPassword(TEST_USER_PASSWORD);
    }

    @Test
    public void testRegister() {
        userService.register(testUser);
    }

    @Test
    public void testAuth(){
        String b = userService.authorize(testUser);
        System.out.println(b);
    }

}
