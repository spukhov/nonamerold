package client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class AbsractTest {
    private Client client = ClientBuilder.newClient();
    public final String URL_BASE = "http://localhost:8080/nonamer/api";


    public Client getClient() {
        return client;
    }


    public void printResponse(Response res) {
        System.out.println("Output from Server .... \n");
        String output = res.readEntity(String.class);
        System.out.println(output);
    }
}
