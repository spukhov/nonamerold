package client;


import com.headlezz.mongo.Credentials;
import com.headlezz.mongo.UserData;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class UserOpsTest extends AbsractTest{
    private Credentials testUser;
    private UserData testUserData;
    private String TEST_USER_EMAIL = "test@test.ua";
    private String TEST_USER_PASSWORD = "testpass";
    private String URL_USER = "/user";
    private String URL_REGISTER = "/register";
    private String URL_AUTH = "/authorize";

    @Before
    public void before(){
        testUser = new Credentials();
        testUser.setEmail(TEST_USER_EMAIL);
        testUser.setPassword(TEST_USER_PASSWORD);
    }
    @Test
    public void testAuth(){
        Response res = getClient().target(URL_BASE + URL_USER + URL_AUTH).
                request(MediaType.APPLICATION_JSON).post(Entity.entity(testUser, MediaType.APPLICATION_JSON));

        System.out.println(res.getHeaderString("token"));
        printResponse(res);
    }

    @Test
    public void testRegister(){
        testUser.setEmail(RandomStringUtils.randomAlphanumeric(5) + testUser.getEmail());
        Response res = getClient().target(URL_BASE + URL_USER + URL_REGISTER).
                request(MediaType.APPLICATION_JSON).post(Entity.entity(testUser, MediaType.APPLICATION_JSON));

        printResponse(res);

    }

}
