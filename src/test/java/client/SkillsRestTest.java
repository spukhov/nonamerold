package client;

import com.headlezz.mongo.Skill;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class SkillsRestTest extends AbsractTest {
    private String URL_SKILLS = "/skills";
    private String URL_SKILL_NAME = "/%s";
    private String URL_SKILL_ADD = "/add";
    private String SPRING = "SPRING";

    @Test
    public void testFindByNameLike(){
        Response res = getClient().target(URL_BASE + URL_SKILLS + String.format(URL_SKILL_NAME, SPRING)).
                request(MediaType.APPLICATION_JSON).get();
        printResponse(res);
    }

    @Test
    public void testAddSkill(){
        Skill skill = new Skill();
        skill.setName(SPRING);

        Response res = getClient().target(URL_BASE + URL_SKILLS + URL_SKILL_ADD).
                request(MediaType.APPLICATION_JSON).post(Entity.entity(skill, MediaType.APPLICATION_JSON));

        printResponse(res);
    }
}
