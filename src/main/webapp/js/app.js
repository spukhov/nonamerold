(function() {
    var app = angular.module('headlezz', []);
    url_skills_get_all="http://8bitcenter.com:8080/nonamer/api/skills/fdf";

    app.controller('SkillController', ['$http', '$log', function($http, $log){

        var user = this;
        user.skills = [];
        $http.get(url_skills_get_all).success(function(data){
            user.skills = data;
        });
    }]);

})();