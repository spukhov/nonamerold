package com.headlezz.model.response;

/**
 * Created by pooh on 28.11.14.
 */
public class AuthResponse extends DefaultResponse {
    public AuthResponse(boolean result, String message) {
        super(result, message);
    }
}
