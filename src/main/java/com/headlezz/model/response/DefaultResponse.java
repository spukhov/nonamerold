package com.headlezz.model.response;

public class DefaultResponse {
    private boolean result;
    private String message;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DefaultResponse(boolean result, String message) {
        this.result = result;
        this.message = message;
    }
}
