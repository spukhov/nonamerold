package com.headlezz.mongo;

import com.headlezz.enums.Language;

import java.util.List;

public class UserData {
    private Language primaryLanguage;
    private List<Skill> skills;

    public Language getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(Language primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
