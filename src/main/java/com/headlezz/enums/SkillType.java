package com.headlezz.enums;

public enum SkillType {
    LANGUAGE, FRAMEWORK, TOOL
}
