package com.headlezz.enums;

public enum Language {
    JAVA, C_SHARP, PHP, C_PLUSPLUS, JAVASCRIPT, PYTHON, OBJECTIVE_C, RUBY, C, PERL, SCALA, ERLANG, GROOVY
}
