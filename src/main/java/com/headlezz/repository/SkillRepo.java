package com.headlezz.repository;

import com.headlezz.mongo.Skill;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillRepo extends MongoRepository<Skill, String> {
    public List<Skill> findByNameLike(String name);
}
