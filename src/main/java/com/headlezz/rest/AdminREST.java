package com.headlezz.rest;

import com.headlezz.model.response.DefaultResponse;
import com.headlezz.mongo.Skill;
import com.headlezz.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

@Path("admin")
public class AdminREST {
    private static final Logger LOG = Logger.getLogger(AdminREST.class.getName());

    @Autowired
    SkillService skillService;

    @POST
    @Path("/skill")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public DefaultResponse addSkill(Skill skill) {
        try {
            skillService.addSkill(skill);
            LOG.info("skill added: " + skill.getName());
            return new DefaultResponse(true, "skill added: " + skill.getName());
        } catch (Exception e) {
            LOG.severe("skill adding FAILED: " + skill.getName());
            return new DefaultResponse(false, e.getMessage());
        }
    }

    @PUT
    @Path("/skill")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public DefaultResponse editSkill(Skill skill){
        try {
            skillService.editSkill(skill);
            LOG.info("skill edited: " + skill.getName());
            return new DefaultResponse(true, "skill edited: " + skill.getName());
        } catch (Exception e) {
            LOG.severe("skill editing FAILED: " + skill.getName());
            return new DefaultResponse(false, e.getMessage());
        }
    }



}
