package com.headlezz.rest;

import com.headlezz.mongo.Credentials;
import com.headlezz.model.response.DefaultResponse;
import com.headlezz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

@Path("user")
public class UserREST {
    private static final Logger LOG = Logger.getLogger(UserREST.class.getName());
    @Autowired
    private UserService userService;

    @POST
    @Path("/authorize")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public DefaultResponse authorize(Credentials creds, @Context HttpServletResponse response) {
        try {
            String token = userService.authorize(creds);
            if (token!=null){
                LOG.info("authorization attempt : (" + true + ") " + creds.getEmail());
                response.setHeader("token", token);
                return new DefaultResponse(true, "user authorized: " + creds.getEmail());
            } else {
                LOG.info("authorization attempt : (" + false + ") " + creds.getEmail());
                return new DefaultResponse(false, "user NOT authorized: " + creds.getEmail());
            }

        } catch (Exception e) {
            LOG.severe("authorization failed with exception for user " + creds.getEmail()
                    + ": " + e.getMessage());
            return new DefaultResponse(false, e.getMessage());
        }
    }


    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public DefaultResponse register(Credentials creds) {
        try {
            userService.register(creds);
            LOG.info("user registered: " + creds.getEmail());
            return new DefaultResponse(true, "user registered: " + creds.getEmail());
        } catch (Exception e) {
            LOG.severe("registering failed with exception for user " + creds.getEmail()
                    + ": " + e.getMessage());
            return new DefaultResponse(false, e.getMessage());
        }
    }

}
