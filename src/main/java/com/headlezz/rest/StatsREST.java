package com.headlezz.rest;

import com.headlezz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("stats")
public class StatsREST {
    @Autowired
    UserService userService;

    @GET
    @Path("/count")
    public long getUsersCount(){
        return userService.getUserCount();
    }
}
