package com.headlezz.rest;

import com.headlezz.model.response.DefaultResponse;
import com.headlezz.mongo.Feedback;
import com.headlezz.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("feedback")
public class FeedbackREST {
    @Autowired
    private FeedbackService feedbackService;

    @POST
    public DefaultResponse leaveFeedback(Feedback feedback) {
        try {
            feedbackService.save(feedback);
            return new DefaultResponse(true, "feedback sent " + feedback.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new DefaultResponse(true, "feedback sending failed with exception: "
                    + feedback.toString() + " | " + e.getMessage());
        }

    }
}
