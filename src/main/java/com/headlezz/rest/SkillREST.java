package com.headlezz.rest;


import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.headlezz.mongo.Skill;
import com.headlezz.service.SkillService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("skills")
public class SkillREST {
    static final Logger LOG = Logger.getLogger(SkillREST.class.getName());

    @Autowired
    private SkillService skillService;

    @GET
    @Path("/all")
    @Produces({MediaType.APPLICATION_JSON})
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    public List<Skill> getAllSkills(@HeaderParam("token") String token) {
        LOG.warn("getting all skills");
        return skillService.getAllSkills();
    }

    @POST
    @Path("/add")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public void addSkill(/*@PathParam("token") String token, */Skill skill) {
        skillService.addSkill(skill);
    }

    @GET
    @Path("/tabs")
    public void getSkillsTabs(){
       skillService.getSkillTabs();
    }

    @GET
    @Path("/{name}")
    public List<Skill> getSkillByName(@PathParam("name") String name){
        return skillService.getSkillsByName(name);

    }
}
