package com.headlezz.service;

import com.headlezz.mongo.Skill;
import com.headlezz.repository.SkillRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillService {
    @Autowired
    private SkillRepo skillRepo;

    public List<Skill> getAllSkills() {
        return skillRepo.findAll();
    }

    public List<Skill> getSkillsByName(String name) {
         return skillRepo.findByNameLike(name);
    }

    public void addSkill(Skill skill) {
        skillRepo.save(skill);

    }

    public void editSkill(Skill skill) {
        skillRepo.findOne(skill.getId());
    }


    public void getSkillTabs() {
        // TODO - skill tabs based on user specialization
    }
}
