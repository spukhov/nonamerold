package com.headlezz.service;

import com.headlezz.mongo.Credentials;
import com.headlezz.mongo.User;
import com.headlezz.repository.UserRepo;
import com.headlezz.util.Encription;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserService {
    private final short TOKEN_SIZE = 20;

    @Autowired
    UserRepo userRepo;

    public String authorize(Credentials creds) {
        User existingUser = userRepo.findOne(creds.getEmail());
        if (Encription.checkPassword(existingUser.getPassword(),
                Encription.encode(creds.getPassword()))) {
            String token = generateToken();
            existingUser.setToken(token);
            userRepo.save(existingUser);
            return token;
        }
        return null;
    }

    public void register(Credentials creds) {
        // TODO: Check email is UNIQUE
        userRepo.save(new User(creds.getEmail(), Encription.encode(creds.getPassword())));
    }

    public long getUserCount() {
        return userRepo.count();
    }

    public String generateToken(){
        return RandomStringUtils.randomAlphanumeric(TOKEN_SIZE);
    }

}
