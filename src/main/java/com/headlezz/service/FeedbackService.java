package com.headlezz.service;

import com.headlezz.mongo.Feedback;
import com.headlezz.repository.FeedbackRepo;
import org.springframework.beans.factory.annotation.Autowired;

public class FeedbackService {
    @Autowired
    private FeedbackRepo feedbackRepo;

    public void save(Feedback feedback){
        feedbackRepo.save(feedback);
    }
}
